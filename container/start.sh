#!/bin/bash

echo "Started integrated Apache2 instance"
apache2ctl  -k start
tail -f /var/log/apache2/access.log
echo "Goodbye"