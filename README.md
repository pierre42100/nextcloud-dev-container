# NextCloud container

This project can be used to run in a Docker container a Nextcloud instance in order to build an application.

Usage :
```bash
mkdir app/
container/build.sh
container/run.sh
```

You need to install NextCloud in the `app` directory then open your browser in http://localhost:8080/


## Enable debug mode
In your NextCloud instance, replace the begining of the file `config/config.php` from 
```php
<?php
$CONFIG = array (
```

to
```php
<?php
$CONFIG = array (
	'debug' => true,
```